import sys
import random
from time import sleep
try:
    from curses import KEY_LEFT, KEY_DOWN, KEY_RIGHT, KEY_UP
    from curses import use_default_colors, wrapper, curs_set
except ImportError:
    print("Pnake requires curses, which is not available in the system!")
    sys.exit(1)

WIDTH = 0
ENDY = 0
HEIGHT = 0
ENDX = 0
STDSCR = 0

cursor_x = 0
cursor_y = 0

def print_(x):
    global cursor_x, cursor_y
    for letter in x:
        if letter == '\n':
            cursor_x = 0
            cursor_y += 1
        else:
            STDSCR.addch(cursor_y, cursor_x, letter)
            cursor_x += 1
        if(letter == '\t'):
            cursor_x += 3
        if(cursor_x >= WIDTH):
            cursor_y += 1
            cursor_x = 0
        if (cursor_y >= HEIGHT):
            cursor_y = 0

LEFT    = 0
RIGHT   = 1
UP      = 2
DOWN    = 3
MOVES   = [(0, -2), (0, 2), (-1, 0), (1, 0)]
MOVESSTR = ["L", "R", "U", "D"]

def next_pixel(pixel, direction):
    distance = MOVES[direction]
    new_x = pixel[1] + distance[1]
    new_y = pixel[0] + distance[0]

    if(new_x >= ENDX):
        new_x = 0
    elif(new_x < 0):
        new_x = ENDX

    if(new_y >= HEIGHT):
        new_y = 0
    elif(new_y < 0):
        new_y = ENDY
    return (new_y, new_x)

class Snake:
    PIXEL   = u"\u2022"
    FACES   = [u"\u21e6", u"\u21e8", u"\u21e7", u"\u21e9"]
    FOOD    = u"\u2b1b"
    def __init__(self):
        self.pixels = [(0, 0), (0, 2), (0, 4)]
        self.face = 1
        self.collided = False
        self.food_at = (0, 0)
        self.food_at = self.next_food()
        self.old_food_at = (0, 0)
        self.last_pixel = (0, 0)

    def next_food(self):
        x = self.pixels[0][1]
        y = self.pixels[0][0]
        while (y, x) in self.pixels:
            y = random.randint(0, ENDY)
            x = random.randrange(0, ENDX, 2)
        return (y, x)

    def move(self, d):
        self.face = d
        new_pixel = next_pixel(self.pixels[-1], d)

        if new_pixel in self.pixels:
            self.collided = True
        eaten = False

        self.pixels.append(new_pixel)
        if new_pixel[0] == self.food_at[0] and new_pixel[1] == self.food_at[1]:
            eaten = True
        if eaten:
            self.food_at = self.next_food()
        else:
            self.last_pixel = self.pixels[0]
            self.pixels = self.pixels[1:]

def draw(snake, stdscr):
    # turn off the last pixel
    stdscr.addstr(snake.last_pixel[0], snake.last_pixel[1], "  ")
    # change the face to body
    stdscr.addstr(snake.pixels[-2][0], snake.pixels[-2][1], "  ")
    stdscr.addstr(snake.pixels[-2][0], snake.pixels[-2][1], snake.PIXEL)
    # turn on the new face
    stdscr.addstr(snake.pixels[-1][0], snake.pixels[-1][1], snake.FACES[snake.face])
    # if the food is changed, turn it on
    if snake.old_food_at != snake.food_at:
        stdscr.addstr(snake.food_at[0], snake.food_at[1], snake.FOOD)
        # mark it as the new food
        snake.old_food_at = snake.food_at
    stdscr.refresh()


class AI:

    def __init__(self, snake):
        self.snake = snake
        self.g_scores = {}
        self.f_scores = {}

    def h(self, current, goal):
        return abs(current[0] - goal[0]) + abs(current[1] - goal[1])

    def g_score(self, vertex):
        if vertex not in self.g_scores:
            return 99999 # arbitrarily large value
        return self.g_scores[vertex]

    def f_score(self, vertex):
        if vertex not in self.f_scores:
            return 99999 # arbitrarily large value
        return self.f_scores[vertex]

    def get_neighbours(self, point):
        return (next_pixel(point, LEFT), next_pixel(point, RIGHT),
                next_pixel(point, UP), next_pixel(point, DOWN))

    def d(self, current, pixel):
        if pixel in self.snake.pixels:
            return 99999 # cannot be reached
        return 1

    def generate_directions(self, cameFrom, current):
        final = []
        while current in cameFrom:
            final.insert(0, cameFrom[current][1])
            current = cameFrom[current][0]
        return final

    # A* search
    # directly copied from Wikipedia
    def generate_moves(self):
        goal = self.snake.food_at
        start = self.snake.pixels[-1]
        #print_("Goal : " +  str(goal) + "Start:" + str(start))
        cameFrom = {}

        self.g_scores = {}
        self.g_scores[start] = 0

        self.f_scores = {}
        self.f_scores[start] = self.h(start, goal)

        openSet = [start]

        while len(openSet) > 0:
            current = min(openSet, key=self.f_score)
            if current == goal:
                directions = self.generate_directions(cameFrom, current)
                #print_("Direction : ")
                #for d in directions:
                #    print_(MOVESSTR[d])
                #print_("\n")
                return directions

            openSet.remove(current)
            curr_gscore = self.g_score(current)

            neighbours = self.get_neighbours(current)
            for direction, neighbour in enumerate(neighbours):
                tentative_gscore = curr_gscore + self.d(current, neighbour)
                if tentative_gscore < self.g_score(neighbour):
                    cameFrom[neighbour] = (current, direction)
                    self.g_scores[neighbour] = tentative_gscore
                    self.f_scores[neighbour] = self.g_score(neighbour) + self.h(neighbour, goal)
                    if neighbour not in openSet:
                        openSet.append(neighbour)
        return []

def main(stdscr):
    global WIDTH, HEIGHT, STDSCR, ENDX, ENDY
    # curses setup
    use_default_colors()
    stdscr.nodelay(True)
    stdscr.clear()
    curs_set(0)
    # globals setup
    HEIGHT, WIDTH = stdscr.getmaxyx()
    ENDX = WIDTH - 2 + (WIDTH % 2)
    ENDY = HEIGHT - 1
    STDSCR = stdscr
    # initialization
    s = Snake()
    ai = AI(s)
    refresh_rate = 60
    sleep_time = 1/refresh_rate
    update_rate = 50
    tick = 0
    direction = RIGHT
    force_update = False
    old_food_at = s.food_at
    old_moves = ai.generate_moves()
    # game loop
    while True:
        tick += 1
        """
        key = stdscr.getch()
        if(key != -1):
            if(key == KEY_LEFT):
                if direction != RIGHT:
                    direction = LEFT
                    force_update = True
            elif(key == KEY_RIGHT):
                if direction != LEFT:
                    direction = RIGHT
                    force_update = True
            elif(key == KEY_UP):
                if direction != DOWN:
                    direction = UP
                    force_update = True
            elif(key == KEY_DOWN):
                if direction != UP:
                    direction = DOWN
                    force_update = True
            elif(key == '+'):
                update_rate += 1
                force_update = True
            elif(key == '-'):
                update_rate -= 1
                force_update = True
            else:
                sys.exit(0)
        """
        if(update_rate >= refresh_rate/tick or force_update):
            s.move(old_moves.pop(0))
            draw(s, stdscr)
            #if len(old_moves) == 0:
            #    stdscr.addstr(0, 0, "No moves")
            #else:
            #    for x, move in enumerate(old_moves):
            #        stdscr.addstr(0, x + 2, str(move) + " ")
            if s.food_at != old_food_at:
                old_moves = ai.generate_moves()
                old_food_at = s.food_at
            force_update = False
            if s.collided or len(old_moves) == 0:
                stdscr.nodelay(False)
                if s.collided:
                    stdscr.addstr(0, 0, "Game Over")
                else:
                    stdscr.addstr(0, 0, "Can't find moves! (food at %s, head at %s)"
                                  % (str(s.food_at), str(s.pixels[-1])))
                stdscr.getch()
                sys.exit(0)
            tick = 0
        sleep(sleep_time)


if __name__ == "__main__":
    wrapper(main)
